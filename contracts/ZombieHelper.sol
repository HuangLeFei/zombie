pragma solidity ^0.8.11;

import "./ZombieFeeding.sol";

/**
 @title 僵尸升级、改名、改dna
 @author lfhuang
 @dev  ”返回某玩家的整个僵尸军团“的函数,无需花费gas
*/
contract ZombieHelper is ZombieFeeding{

    uint levelUpFee = 0.001 ether;//升级所需手续费

    //函数修饰符判断僵尸等级
    modifier aboveLevel(uint _level,uint _zombieId){
        require(zombies[_zombieId].level >= _level);
        _;//修饰符的最后一行为 _;，表示修饰符调用结束后返回，并执行调用函数余下的部分。
    }

    //提现以太坊函数
    function withdraw() external onlyOwner{
        payable(msg.sender).transfer(address(this).balance);
    }

    //设置僵尸升级所需手续费
    function setLevelUpFee(uint _fee) external onlyOwner{
        levelUpFee = _fee;
    }

    //僵尸升级函数，需要支付eth
    function levelUp(uint _zombieId) external payable{
        require(msg.value == levelUpFee);//判断支付金额是不是升级所需手续费
        zombies[_zombieId].level ++;
    }

    /**
    游戏玩法：
    2级以上的僵尸，玩家可给他们改名。
    */
    function changeName(uint _zombieId,string memory _newName) external aboveLevel(2,_zombieId) onlyOwnerOf(_zombieId){
        //require(payable(msg.sender) == zombieToOwner[_zombieId]);
        zombies[_zombieId].name = _newName;
    }
    /**
    游戏玩法：
    20级以上的僵尸，玩家能给他们定制的 DNA。
    */
    function changeDna(uint _zombieId,uint _newDna) external aboveLevel(20,_zombieId) onlyOwnerOf(_zombieId){
        //require(payable(msg.sender) == zombieToOwner[_zombieId]);
        zombies[_zombieId].dna = _newDna;
    }

    //”返回某玩家的整个僵尸军团“的函数,无需花费gas
    function getZombiesByOwner(address _owner) external view returns(uint[] memory){
        uint[] memory result = new uint[](ownerZombieCount[_owner]);
        uint counter = 0;//数组下标
        //遍历所有僵尸的总数
        for(uint i = 1; i <= zombies.length; i++){
            if(zombieToOwner[i] == _owner){//数组下标找到对应的地址和参数地址进行匹配，记录当前地址的总数
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }
}