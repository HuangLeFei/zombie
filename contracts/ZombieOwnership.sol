pragma solidity ^0.8.11;

import "./ZombieBattle.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
//import "./ERC721.sol";

/**
 @title 一个管理转移僵尸所有权的合约
 @author lfhuang
 @dev 符合 OpenZeppelin 对 ERC721 标准草案的实现
*/
contract ZombieOwnership is ZombieBattle,ERC721{
    //即使您不想更改原来的 ERC721 构造函数，您的合约中仍然需要一个。
    constructor() ERC721("GameItem", "ITM") public { }

    mapping (uint => address) zombieApprovals;

    function balanceOf(address _owner) public view virtual override  returns (uint256 _balance){
        // 1. 在这里返回 `_owner` 拥有的僵尸数
        return ownerZombieCount[_owner];
    }

    function ownerOf(uint256 _tokenId) public view virtual override returns (address _owner) {
        // 2. 在这里返回 `_tokenId` 的所有者
        return zombieToOwner[_tokenId];
    }

    //定义 _transfer 的逻辑。
    function _transfers(address _from,address _to,uint256 _tokenId) private{
        ownerZombieCount[_from]--;
        ownerZombieCount[_to]++;
        zombieToOwner[_tokenId] = _to;
        emit Transfer(_from,_to,_tokenId);//ERC721规范转账
    }

    function transfer(address _to, uint256 _tokenId) public onlyOwnerOf(_tokenId){
         _transfers(msg.sender, _to, _tokenId);
    }

    function approve(address _to, uint256 _tokenId) public override onlyOwnerOf(_tokenId){
        zombieApprovals[_tokenId] = _to;
        emit Approval(msg.sender, _to, _tokenId);
    }

    // function getApproved(uint256 _tokenId) public view virtual override returns (address) {
    //     require(zombieApprovals[_tokenId] == msg.sender);
    //     address owner = ownerOf(_tokenId);
    //     _transfer(owner, msg.sender, _tokenId);
    // }

    function takeOwnership(uint256 _tokenId) public {
        require(zombieApprovals[_tokenId] == msg.sender);
        address owner = ownerOf(_tokenId);
        _transfer(owner, msg.sender, _tokenId);
    }
}