pragma solidity ^0.8.11;
import "./ZombieHelper.sol";

/**
 @title 僵尸攻击
 @author lfhuang
 @dev  keccak256生成随机数，僵尸攻击作战
*/
contract ZombieBattle is ZombieHelper{
    uint randNonce = 0;//自增序号
    uint attackVictoryProbability = 70;//攻击获胜概率

    //keccak256生成随机数，此方法很容易被不诚实的节点攻击
    function randMod(uint _modulus) internal returns(uint){
        randNonce++;
        uint random = uint(keccak256(abi.encodePacked(block.timestamp, payable(msg.sender), randNonce))) % _modulus;
    }

    //僵尸攻击，僵尸id,攻击目标僵尸id
    function attack(uint _zombieId,uint _targetId) external onlyOwnerOf(_zombieId){
        //require(msg.sender == zombieToOwner[_zombieId]);//检查拥有者权限,多次使用判断，修改使用modifier 中来清理代码并避免重复编码。
        Zombie storage myZombie = zombies[_zombieId];
        Zombie storage enemyZombie = zombies[_targetId];
        uint rand = randMod(100);
        if(rand <= attackVictoryProbability){
            myZombie.winCount++;
            myZombie.level++;
            enemyZombie.lossCount++;
            feedAndMultiply(_zombieId, enemyZombie.dna, "zombie");//生成新的僵尸
        }else{
            myZombie.lossCount++;
            enemyZombie.winCount++;
            _triggerCooldown(myZombie);//重置冷却时间，每天只能参战一次
        }
    }
}