pragma solidity ^0.8.11;
import "./ZombieFactory.sol";

//定义接口与CryptoKitties合约进行交互
interface KittyInterface{
    function getKitty(uint256 _id) external view returns (
        bool isGestating,
        bool isReady,
        uint256 cooldownIndex,
        uint256 nextActionAt,
        uint256 siringWithId,
        uint256 birthTime,
        uint256 matronId,
        uint256 sireId,
        uint256 generation,
        uint256 genes
    );
}

/**
 @title 僵尸喂食、攻击
 @author lfhuang
 @dev  生成新的僵尸,动态调用外部合约
*/
contract ZombieFeeding is ZombieFactory{

    //address ckAddress = 0x06012c8cf97BEaD5deAe237070F9587f8E7A266d;
    //KittyInterface kittyContract = KittyInterface(ckAddress);//通过接口进行CryptoKitties合约交互
    
    KittyInterface kittyContract;

    modifier onlyOwnerOf(uint _zombieId){
        require(msg.sender == zombieToOwner[_zombieId]);//确保僵尸主人操作
        _;
    }

    //CryptoKitties 合约地址,动态指定更改地址,指定合约所有权是自己
    function setKittyContractAddress(address _address) external onlyOwner{
        kittyContract = KittyInterface(_address);
    }
    
    function feedOnKitty(uint _zombieId,uint _kittyId) public{
        uint kittyDna;
        (,,,,,,,,,kittyDna) = kittyContract.getKitty(_kittyId);//处理返回多个值(获取加密猫的基因)
        feedAndMultiply(_zombieId,kittyDna,"kitty");
    }

    function feedAndMultiply(uint _zombieId,uint _targetDna,string memory _species) internal onlyOwnerOf(_zombieId){
        //require(msg.sender == zombieToOwner[_zombieId]);//确保僵尸主人操作,多次使用判断，修改使用modifier 中来清理代码并避免重复编码。
        Zombie storage myZombie = zombies[_zombieId];
        _isReady(myZombie);//检查僵尸的冷却周期，在执行后面操作
        _targetDna = _targetDna % dnaModulus;
        uint newDna = (myZombie.dna + _targetDna) / 2;
        //比较字符串，需要比较他们的 keccak256 哈希码
        if(keccak256(abi.encodePacked(_species)) == keccak256(abi.encodePacked("kitty"))){
            newDna = newDna - newDna % 100 + 99;
        }
        _createZombie("NoName", newDna);
        _triggerCooldown(myZombie);//重新设置僵尸的冷却时间
    }

    //触发冷却时间
    function _triggerCooldown(Zombie memory _zombie) internal{
        _zombie.readyTime = uint32(block.timestamp + cooldownTime);
    }
    
    //计算返回，判断下次允许猎食的时间是否已经到了
    function _isReady(Zombie memory _zombie) internal returns(bool){
       return (block.timestamp > _zombie.readyTime);
    }
}