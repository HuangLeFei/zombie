pragma solidity ^0.8.11;
import "@openzeppelin/contracts/access/Ownable.sol";
// import "@openzeppelin/contracts/utils/math/SafeMath.sol";

/**
 @title 僵尸工厂
 @author lfhuang
 @dev  创建僵尸功能
*/
contract ZombieFactory is Ownable{

    //using SafeMath for uint256;

    uint dnaDigits = 16;//定义僵尸DNA
    uint dnaModulus = 10 ** dnaDigits;//Zombie 的 DNA 只有 16 个字符，让我们使另一个uint等于 10^16。这样我们以后就可以使用模运算符%将整数缩短为 16 位。
    uint cooldownTime = 1 days;
    
    event NewZombie(uint zombieId,string name,uint dna);

    mapping (uint => address) public zombieToOwner;//id 存储和查找僵尸 返回地址
    mapping (address => uint) ownerZombieCount;//记录某地址所拥有僵尸的数量

    struct Zombie{
        string name;
        uint dna;
        uint16 winCount;//2^16 = 65536 除非一个僵尸连续179年每天作战，否则我们就是安全的。
        uint16 lossCount;
        uint32 level;
        uint32 readyTime;//冷却周期
    }

    Zombie[] public zombies;//所有僵尸总数，数组下标id对应僵尸对象结构

    //创建僵尸,具体实现
    function _createZombie(string memory _name,uint _dna) internal{
        // Zombie memory zombie= Zombie(_name,_dna);
        // zombies.push(zombie);
        //  array.push() 返回数组的长度类型是uint - 因为数组的第一个元素的索引是 0， array.push() - 1 将是我们加入的僵尸的索引。 zombies.push() - 1 就是 id，数据类型是 uint。
        // uint id = zombies.push(Zombie(_name, _dna)) - 1;
        zombies.push(Zombie(_name,_dna,0,0,1,uint32(block.timestamp + cooldownTime)));
        uint id = zombies.length-1;
        zombieToOwner[id] = payable(msg.sender);
        ownerZombieCount[payable(msg.sender)]++;
        // 触发事件，通知app
        emit NewZombie(id ,_name,_dna);
    }

    //根据僵尸名字生成dna
    function _generateRandomDna(string calldata _str) private view returns(uint){
        uint rand = uint(keccak256(abi.encodePacked(_str)));
        return rand % dnaModulus;//确保DNA的长度为16位
    }

    //创建僵尸
    function createRandomZombie(string calldata _name) public{
        require(ownerZombieCount[payable(msg.sender)]==0, "Only one zombie can be created.");//一个地址只能创建一个僵尸
        uint randDna = _generateRandomDna(_name);
        _createZombie(_name,randDna);
    }
}

// contract ZombieFeeding is ZombieFactory{

// }